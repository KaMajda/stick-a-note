class ApplicationController < ActionController::Base
  helper_method :current_user
  def current_user
    if session[:user_id]
      @current_user  = User.find(session[:user_id])
    end
  end
  def current_table
    if session[:table]
      if Table.exists?(id: session[:table])
        @current_table = Table.find(session[:table])
      end
    end
  end
  def current_user_id
    if session[:user_id]
      @current_user = User.find(session[:user_id])
      @current_user = @current_user.id
    end
  end
  def current_table_id
    if session[:table]
      @current_table = User.find(session[:table])
      @current_table = @current_table.id
    end
  end
  def log_in(user)
    session[:user_id] = user.id
    @current_user = user
    redirect_to root_path
  end

  def logged_in?
    !current_user.nil?
  end

  def log_out
    session.delete(:user_id)
    @current_user = nil
  end


  #code generator l - how many letters
  def inv_code(l)
    code = (0...l).map { (65 + rand(26)).chr }.join
  end
end
