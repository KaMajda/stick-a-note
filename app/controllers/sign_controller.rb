class SignController < ApplicationController
  def signin
  end

  def signup
  end
  def find
    @user = User.find_by(username: params[:signin][:username], password: params[:signin][:password])
    if @user
      log_in(@user)
    end
  end
  def account
    @user_id = current_user_id
    redirect_to "/signin" unless @user_id
    @tables = Table.where(user_id: @user_id)
    @user=User.find(@user_id)
  end
  def create
    if params[:signup][:password] == params[:signup][:c_password]
      @user = User.create(username: params[:signup][:username], password: params[:signup][:password])
      log_in(@user)
    end
  end
  def logout
    log_out
    redirect_to root_path
  end
end
