class TablesController < ApplicationController
  def index
    @table = current_table
    @user = current_user
    redirect_to root_path unless Permition.find_by(user_id: @user.id, table_id: @table.id) or @table.user_id == @user.id
    redirect_to "/signin" unless @user
    @notes = Note.where(table_id: @table.id)
  end

  def show
    @user = current_user
    redirect_to "/signin" unless @user
    @tables = Table.all
  end
  def create
    @user = current_user
    if params[:c_table][:title] != ''
      @table = Table.create(user_id: @user.id, title: params[:c_table][:title], private: params[:c_table][:private], inv: inv_code(16))
      redirect_to '/table/' + @table.id.to_s
    end
    if params[:join][:inv] != ''
      if @table = Table.find_by(inv: params[:join][:code])
        @permition = Permition.create(user_id: @user.id, table_id: @table.id)
        redirect_to '/table/' + @table.id.to_s
      end
    end
  end
  def delete_table
    @user = current_user
    @table = current_table
    if @user.id == @table.user_id
      id = params['table']
      Table.find_by(id: id)&.destroy
    end
    redirect_to root_path

  end
  def create_note
    @table = Table.find(session[:table])
    @note = Note.create(title: params[:note][:title], body: params[:note][:body], table_id: @table.id)
    redirect_to '/table'
  end
  def delete_note
    @user = current_user
    @table = current_table
    if @user.id == @table.user_id or (Permision.find_by(user_id: @user.id, table_id: @table.id) and @table.private == false)
      Note.find_by(params['note'])&.destroy
    end
    redirect_to "/table"
  end
  def open
    session[:table] = params['name']
    @user = current_user
    if Table.exists?(id: session[:table])
      @table=current_table
      redirect_to '/table'
    else
      redirect_to root_path
    end
  end
end
