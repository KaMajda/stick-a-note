class Note < ApplicationRecord
  belongs_to :table
  after_create_commit { broadcast_append_to self.table }

end
