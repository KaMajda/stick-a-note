class Table < ApplicationRecord
  belongs_to :user
  has_many :notes, :dependent => :destroy
  after_create_commit {broadcast_append_to "tables"}
end
