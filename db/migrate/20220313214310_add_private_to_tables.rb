class AddPrivateToTables < ActiveRecord::Migration[7.0]
  def change
    add_column :tables, :private, :boolean
  end
end
