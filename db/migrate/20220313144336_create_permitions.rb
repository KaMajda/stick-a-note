class CreatePermitions < ActiveRecord::Migration[7.0]
  def change
    create_table :permitions do |t|
      t.references :user, null: false, foreign_key: true
      t.references :table, null: false, foreign_key: true

      t.timestamps
    end
  end
end
