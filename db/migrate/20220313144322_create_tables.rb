class CreateTables < ActiveRecord::Migration[7.0]
  def change
    create_table :tables do |t|
      t.references :user, null: false, foreign_key: true
      t.string :title
      t.string :inv

      t.timestamps
    end
  end
end
