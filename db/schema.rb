# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema[7.0].define(version: 2022_03_13_214310) do
  create_table "notes", force: :cascade do |t|
    t.integer "table_id", null: false
    t.string "title"
    t.string "body"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["table_id"], name: "index_notes_on_table_id"
  end

  create_table "permitions", force: :cascade do |t|
    t.integer "user_id", null: false
    t.integer "table_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["table_id"], name: "index_permitions_on_table_id"
    t.index ["user_id"], name: "index_permitions_on_user_id"
  end

  create_table "tables", force: :cascade do |t|
    t.integer "user_id", null: false
    t.string "title"
    t.string "inv"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "private", default: false
    t.index ["user_id"], name: "index_tables_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "username"
    t.string "password"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_foreign_key "notes", "tables"
  add_foreign_key "permitions", "tables"
  add_foreign_key "permitions", "users"
  add_foreign_key "tables", "users"
end
